import appReducer from "./reducers";
import {applyMiddleware, compose, createStore} from "redux";
import thunk from "redux-thunk";

const store = createStore(
    appReducer, undefined, compose(applyMiddleware(thunk))
);

export default store;