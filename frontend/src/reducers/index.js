import { SCORE_POSTED, GET_SCORES, POST_SCORE, SCORES_RECEIVED} from '../actions/action_types'

const initialState = {
    scoresReceived: false,
    postingNewScore: false,
    postedNewScore: false,
    scores: []
};

const appReducer = (state = initialState, action) => {
    if (action.type ===GET_SCORES){
        return {...state,
            scoresReceived: false
        }
    }

    if (action.type ===SCORES_RECEIVED){
        return {...state,
            scoresReceived: true,
            scores: action.payload
        }
    }

    if (action.type ===POST_SCORE){
        return {...state,
            postingNewScore: true,
            postedNewScore: false
        }
    }

    if (action.type ===SCORE_POSTED){
        return {...state,
            postingNewScore: false,
            postedNewScore: true
        }
    }

    return state;
};

export default appReducer;