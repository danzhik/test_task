import React, {Suspense} from 'react';
import './App.css';
import i18n from './i18n/index'
import {I18nextProvider} from "react-i18next";
import Home from "./components/Home";
import {StoreContext} from 'redux-react-hook';
import Provider from "react-redux/lib/components/Provider";

const App = ({store}) => {
  return (
      <Suspense fallback={<p>&nbsp;</p>}>
        <I18nextProvider i18n={i18n}>
            <Provider store={store}>
            <StoreContext.Provider value={store}>
                <Home/>
            </StoreContext.Provider>
            </Provider>
        </I18nextProvider>
      </Suspense>
  );
}

export default App;
