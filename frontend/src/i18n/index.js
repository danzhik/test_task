import i18n from 'i18next';
import {initReactI18next} from "react-i18next";

const namespaces = ['default'];

i18n.use(initReactI18next)
    .init({
        lng: 'gb',
        ns: namespaces,
        debug: false,
        defaultNS: 'default',
        fallbackLng: 'gb',
        interpolation: {
            escapeValue: false, // not needed for react as it escapes by default
        },
    }).then();

export default i18n;