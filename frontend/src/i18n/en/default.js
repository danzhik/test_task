const locale =  {
  "welcome": "Welcome to amazing scoreboard by Daniil Zhitnitskii!",
  "score": "Score",
  "username" : "Username",
  "post" : "Add!",
  "changeLanguage": "Switch language!",
  "addNew": "Got a new score? Add it with a form below!",
};

export default locale;