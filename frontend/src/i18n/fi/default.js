const locale =  {
  "welcome": "Tervetuloa Daniil Zhitnitskiin hämmästyttävään tulostauluun!",
  "score": "Lopputulos",
  "username" : "Nimi",
  "post" : "Lisää!",
  "changeLanguage": "Vaihda kieli!",
  "addNew": "Onko sinulla uusi Lopputulos? Lisää se alla olevaan lomakkeeseen!"
};

export default locale;