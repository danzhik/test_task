export const GET_SCORES = "GET_SCORES";

export const SCORES_RECEIVED = "SCORES_RECEIVED";

export const POST_SCORE = "POST_SCORE";

export const SCORE_POSTED = "SCORE_POSTED";