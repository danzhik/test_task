import { SCORE_POSTED, GET_SCORES, POST_SCORE, SCORES_RECEIVED} from './action_types'
import axios from 'axios';
import backendUrl from "../config";

export const getScores = () => (dispatch) => {
    dispatch({type: GET_SCORES});
    axios.get(backendUrl + "/scoreboard").then((res) => {
        dispatch ({type: SCORES_RECEIVED, payload: res.data});
    }).catch((err) => {
        console.log("Uuupsie. Loading failed :(");
    })
};
export const postScore = (score, callback) => (dispatch) => {
    dispatch({type: POST_SCORE});
    axios.post(backendUrl + "/scoreboard", score).then((res) => {
        dispatch ({type: SCORE_POSTED});
        dispatch ({type: SCORES_RECEIVED, payload: res.data});
        if (callback && typeof callback === "function") {
            callback();
        }
    }).catch((err) => {
        console.log("Uuupsie. Posting failed :(");
    })
};