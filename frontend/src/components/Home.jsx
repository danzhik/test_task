import {useTranslation} from "react-i18next";
import React, {useCallback, useState} from "react";
import Table from "./Table";
import { Button } from "react-bootstrap";
import i18n from '../i18n/index'
import FlagIcon from "../i18n/flag_icon";
import Form from "react-bootstrap/Form";
import {useDispatch} from "redux-react-hook";
import {postScore} from "../actions";

const Home = ({store}) => {

    const { t } = useTranslation('default');

    const [username, setUsername] = useState( "" );
    const [score, setScore] = useState( "" );

    const onClick = (e) => {
        if (i18n.language === 'fi') {
            i18n.changeLanguage('gb');
        } else {
            i18n.changeLanguage('fi');
        }
    }

    const dispatch = useDispatch();

    const clearFields = () => {
        setUsername("");
        setScore("");
    };

    const sendScore = useCallback(
        () => {
            dispatch(postScore({username, score}, clearFields));
        }, [dispatch, {username, score}]);

    const updateInputValueUsername = (e) => {
        setUsername(e.target.value);
    };
    const updateInputValueScore = (e) => {
        setScore(e.target.value);
    };


    return <div className={"home"}>
                <h1>{t("welcome")}</h1>
                <Button disabled={false} onClick={onClick}>
                    <FlagIcon code={i18n.language}/>
                    &nbsp;{ t('changeLanguage')}
                </Button>
                <br/><br/>
                <Table t={ t }/>
                <br/>
                <h2>{t("addNew")}</h2>
        <Form.Control onChange={updateInputValueUsername} value={username}  type="text" placeholder={t("username")} style={{width: "200px", display: "inline-block"}} />
        <Form.Control onChange={updateInputValueScore} value={score} type="number" placeholder={t("score")} style={{width: "200px", display: "inline-block"}} />
        <br/><br/>
        <Button disabled={false} onClick={sendScore}>
           { t('post')}
        </Button>
            </div>;
};

export default Home;