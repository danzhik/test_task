import React, {Component} from "react";
import PropTypes from "prop-types";
import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';

class Table extends Component {

    constructor(props) {
        super(props);
    }

    componentWillMount() {
       const { loadScores } = this.props;
       loadScores();
    }

    componentDidMount() {
        const { scores } = this.props;
        this.initialScores = scores;
    }

    render() {
        const { t, scores } = this.props
        const columns = [
            { field: 'username', headerName: t("username")},
            { field: 'score', headerName: t("score")}
            ];

        return <div className="ag-theme-alpine">
            <AgGridReact
                api={"sizeColumnsToFit"}
                domLayout={"autoHeight"}
                colWidth={window.innerWidth/2}
                enableSorting={true}
                columnDefs={columns}
                rowData={scores}
            />
        </div>
    }
}

Table.propTypes = {
    scoresReceived: PropTypes.bool.isRequired,
    scores: PropTypes.array.isRequired,
    t: PropTypes.func.isRequired,
    loadScores: PropTypes.func.isRequired
}

export default Table;