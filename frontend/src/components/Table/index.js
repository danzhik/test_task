import {getScores} from "../../actions";
import connect from "react-redux/lib/connect/connect";
import Table from "./Table";

const mapStateToProps = state => ({
    scoresReceived: state.scoresReceived,
    scores: state.scores
});
const mapDispatchToProps = dispatch => ({
    loadScores: () => dispatch(getScores())
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Table);