import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import 'bootstrap/dist/css/bootstrap.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import store from "./store";
import i18n from './i18n/index'
import enLocale from './i18n/en/default'
import fiLocale from './i18n/fi/default'

window.store = store;

//configure i18n
i18n.addResourceBundle('gb', 'default', enLocale, true);
i18n.addResourceBundle('fi', 'default', fiLocale, true);

ReactDOM.render(
        <App store = { store }/>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
