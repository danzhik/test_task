##Hello dear Anders Innovatoins!
Here is small instruction on how to start the task app you requested. The project consists of two parts: frontend and backend. 
In order to run the app you will need to start both. NB In order for app to start and properly work ports 3000 and 8081 should be free.

## Required system dependencies
1. Java 11 runtime environment
1. Node v10.15.1

## How to run
1. Run ```npm install``` and then ```npm start``` commands in fronted folder.
1. Run backend part of the app with IDE as Maven project or run with Maven  from command line using ```mvnw spring-boot:run```
in backend folder on Windows or ```./mvnw spring-boot:run``` on MacOS or Linux. NB running spring app from command line requires Maven installed 
on OS, in Windows case PATH variable should be specified as well.