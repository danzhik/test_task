package com.dz.Task.api;

import com.dz.Task.dto.ScoreDTO;
import com.dz.Task.models.Score;
import com.dz.Task.services.ScoreBoardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/scoreboard", produces = MediaType.APPLICATION_JSON_VALUE)
public class ScoreBoardController {

    @Autowired
    private ScoreBoardService scoreBoardService;

    @GetMapping(value = "")
    public List<ScoreDTO> getAllScores () {
        return scoreBoardService.getAllScores();
    }

   @PostMapping(value = "", consumes = {"application/json"})
    public List<ScoreDTO> addScore (@RequestBody ScoreDTO scoreDTO) {
        return scoreBoardService.addScore(scoreDTO);
    }
}
