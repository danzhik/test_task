package com.dz.Task.dto;

import com.dz.Task.models.Score;
import lombok.Data;

@Data
public class ScoreDTO {

    private String username;
    private Long score;

    public ScoreDTO(){}

    public ScoreDTO (Score score) {
        this.score = score.getScore();
        this.username = score.getUsername();
    }
}
