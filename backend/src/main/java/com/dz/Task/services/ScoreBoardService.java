package com.dz.Task.services;

import com.dz.Task.dto.ScoreDTO;
import com.dz.Task.models.Score;
import com.dz.Task.persistance.ScoresRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ScoreBoardService {

    @Autowired
    private ScoresRepository scoresRepository;

    public List<ScoreDTO> getAllScores() {
        return scoresRepository.findAll().stream()
                .map(ScoreDTO::new).collect(Collectors.toList());
    }

    public List<ScoreDTO> addScore(ScoreDTO scoreDTO) {
        Score score = new Score();
        score.setUsername(scoreDTO.getUsername());
        score.setScore(scoreDTO.getScore());
        scoresRepository.save(score);
        return scoresRepository.findAll().stream()
                .map(ScoreDTO::new).collect(Collectors.toList());
    }
}
