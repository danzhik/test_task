package com.dz.Task.persistance;

import com.dz.Task.models.Score;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ScoresRepository extends JpaRepository<Score, Long> {
}
