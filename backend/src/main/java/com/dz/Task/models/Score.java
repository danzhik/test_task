package com.dz.Task.models;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table
public class Score {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    private Long id;

    @Column
    private String username;

    @Column
    private Long score;
}
